import java.util.*;
//import java.util.function.*;

public class Customer{
    public String name;
    public String address;
    public boolean active;
    static Customer a = new Customer("joao", "avenida", true);
    static Customer b = new Customer("pedro", "rua", false);
    public Customer(String name, String address, boolean active){
        this.name = name;
        this.address = address;
        this.active = active;
    }


    public static List<String> getActiveByField(Function<Customer,String> fieldGetter){
        List<String> customers = new ArrayList<>();
        for(Customer customer: getCostumers()){
            if(customer.active){
                customers.add(fieldGetter.apply(customer));
            }
        }
        return customers;
    }
    public static List<Customer> getCostumers(){
        return Arrays.asList(a, b);
    }

}