public class FunctionalInterface {
        
    public static void main(String[] args) {
        System.out.println(Customer.getActiveByField(new Function<Customer, String>(){
            public String apply(Customer t){
                return t.name;
            }
        }));
        // System.out.println(Customer.getActiveByField((Customer customer)-> customer.name));
        // System.out.println(Customer.getActiveByField((Customer customer)-> customer.address));
    }

}
