    public interface Function<T, T1>{
        public T1 apply(T t);
    }